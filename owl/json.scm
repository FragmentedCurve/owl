;; Based on RFC8259

;; value mapping
;;   true, false = #t, #f
;;   null = 'null
;;   [a, b, c] = (vector a b c)
;;   {"foo": "bar" ...} = ((foo "bar") ...)

(define-library (owl json)

   (import
      (owl core)
      (owl math)
      (owl list)
      (owl list-extra)
      (owl string)
      (owl symbol)
      (owl vector)
      (owl lazy)
      (owl proof)
      (prefix (owl parse) get-)
      (only (owl parse) syntax-error? report-syntax-error))

   (export
      parse-json         ;; (string | byte-list-with-utf8 | byte-stream-with-utf8) → value | 'error
      parse-json-or      ;; as with parse-json, but retun a given error value instead of 'error on error
      byte-stream->json-stream)

   (begin

      (define get-ws
         (get-byte-if
            (λ (x)
               (or (eq? x #x20)
                   (eq? x #x09)
                   (eq? x #x0a)
                   (eq? x #x0d)))))

      (define maybe-whitespace
         (get-greedy-star get-ws))

      (define required-whitespace
         (get-plus get-ws))

      (define get-digit
         (get-byte-if
            (λ (x)
               (and
                  (<= #\0 x)
                  (<= x #\9)))))

      (define get-sign
         (get-either
            (get-parses
               ((skip (get-imm #\-)))
               -1)
            (get-epsilon +1)))

      (define (chars->num chars)
         (fold (λ (n c) (+ (* n 10) (- c #\0))) 0 chars))

      (define get-decimal-part
         (get-parses
            ((skip (get-imm #\.))
             (chars (get-greedy-plus get-digit)))
            (/ (chars->num chars)
               (expt 10 (length chars)))))

      (define get-exponent
         (get-parses
            ((skip (get-byte-if (λ (x) (or (eq? x #\e) (eq? x #\E)))))
             (chars (get-greedy-plus get-digit)))
            (expt 10 (chars->num chars))))

      (define get-number
         (get-parses
            ((sign get-sign)
             (chars
               (get-greedy-plus get-digit))
             (decimal-part
                (get-either get-decimal-part (get-epsilon 0)))
             (exponent
                (get-either get-exponent (get-epsilon 1))))
            (* (* sign (+ (chars->num chars) decimal-part)) exponent)))


      (define get-list-end
         (get-parses
            ((skip (get-imm 93)))
            '()))

      (define (get-list-first get-json)
         (get-parses
            ((skip maybe-whitespace)
             (vals (get-either! get-list-end   ;; empty list
                      (get-parses
                         ((val (get-json))
                          (tail (get-either! get-list-end  ;; list with one value
                                   (get-parses
                                      ((vals (get-greedy-plus
                                               (get-parses
                                                  ((skip (get-imm #\,))
                                                   (val (get-json)))
                                                  val)))
                                       (skip get-list-end))
                                      vals))))
                        (cons val tail)))))
            vals))

      (define (hex-val x)
         (cond
            ((lesser? x #\0) #f)
            ((lesser? x 58) (- x #\0))
            ((lesser? x #\A) #f)
            ((lesser? x 71) (- x 55))
            ((lesser? x 97) #f)
            ((lesser? x 103) (- x 87))
            (else #f)))

      (define get-hex
         (get-parses
            ((val (get-byte-if hex-val)))
            (hex-val val)))

      (define get-4hex
         (get-parses
            ((a get-hex)
             (b get-hex)
             (c get-hex)
             (d get-hex))
            (bior (bior (<< a 12) (<< b 8)) (bior (<< c 4) d))))

      (define (combine-surrogates hi lo)
         (+ #x10000
            (bior
               (<< (- hi #xd800) 10)
               (- lo #xdc00))))

      (define (low-surrogate? x)
         (cond
            ((< x #xdc00) #f)
            ((> x #xdfff) #f)
            (else #t)))

      (define (high-surrogate? x)
         (cond
            ((< x #xd800) #f)
            ((> x #xdbff) #f)
            (else #t)))

      (define get-low-surrogate
         (get-parses
            ((skip (get-imm #\\))
             (skip (get-imm #\u))
             (val get-4hex)
             (verify (low-surrogate? val) "expected a low unicode surrogate"))
            val))

      (define get-quoted-char
         (get-parses
            ((skip (get-imm #\\))
             (val
               (get-either!
                  (get-parses
                     ((skip (get-imm #\u))
                      (val get-4hex)
                      (verify (not (low-surrogate? val)) "invalid lone low unicode surrogate")
                      (low (if (high-surrogate? val) get-low-surrogate (get-epsilon #f))))
                     (if low
                        (combine-surrogates val low)
                        val))
                  (get-parses
                     ((char get-rune))
                     (cond
                        ((eq? char #\") #x22) ;; quotation mark
                        ((eq? char #\\) #x5c) ;; reverse solidus
                        ((eq? char #\/) #x2f) ;; solidus
                        ((eq? char #\b) #x08) ;; backspace
                        ((eq? char #\f) #x0c) ;; form feed
                        ((eq? char #\n) #x0a) ;; line feed
                        ((eq? char #\r) #x0d) ;; carriage return
                        ((eq? char #\t) #x09) ;; tab
                        (else char))))))
               val))


      (define get-string-char
         (get-one-of!
            get-quoted-char
            (get-rune-if
               (λ (x)
                  (and (not (eq? x #\"))
                       (not (eq? x #\\)))))))

      (define get-string
         (get-parses
            ((skip (get-imm 34))
             (runes (get-greedy-plus get-string-char))
             (skip (get-imm 34)))
            (list->string runes)))

      (define get-object-end
         (get-parses
            ((skip (get-imm 125)))
            '()))

      (define (get-key-value get-json)
         (get-parses
            ((skip maybe-whitespace)
             (k get-string)
             (skip (get-imm #\:))
             (v (get-json))
             (skip maybe-whitespace))
            (cons (string->symbol k) v)))

      (define (get-object-first get-json)
         (get-parses
            ((skip maybe-whitespace)
             (vals
                (get-either!
                   get-object-end   ;; {}
                   (get-parses
                      ((pair (get-key-value get-json))
                       (tail (get-either! get-object-end
                              (get-parses
                                 ((vals (get-greedy-plus
                                          (get-parses
                                             ((skip (get-imm #\,))
                                              (val (get-key-value get-json)))
                                             val)))
                                  (skip get-object-end))
                                 vals))))
                     (cons pair tail)))))
            vals))

      ;; json object is one of (false, null, true), object, array, number or a string
      (define (get-json)
         (get-parses
            ((skip maybe-whitespace)
             (val
                (get-one-of!
                  (get-word "true" #t)
                  (get-word "false" #f)
                  (get-word "null" 'null)
                  get-number
                  get-string
                  (get-parses
                     ((skip (get-imm 91))
                      (vals (get-list-first get-json)))
                     (list->vector vals))
                  (get-parses
                     ((skip (get-imm 123))
                      (vals (get-object-first get-json)))
                     vals) ;; an association list
                  ))
             ;(skip maybe-whitespace) ;; avoid this to be able to stream expressions
             )
            val))


      (define whitespace-value "ws") ;; anything eq?

      (define get-json-or-whitespace
         (get-either
            (get-json)
            (get-parses
               ((skip required-whitespace))
               whitespace-value)))

      (define get-json-with-trailing-whitespace
         (get-parses
            ((val (get-json))
             (skip maybe-whitespace))
            val))

      (define (parse-json x)
         (cond
            ((string? x)
               (parse-json (str-iter x)))
            (else
               (get-parse get-json-with-trailing-whitespace x 'error))))

      (define (byte-stream->json-stream ll)
         (lkeep
            (lambda (result)
               (cond
                  ((eq? result whitespace-value)
                     ;; drop a plain whitespace value, which is allowed at end of file
                     #f)
                  ((syntax-error? result)
                     (report-syntax-error result)
                     #f)
                  (else
                     #t)))
            (get-byte-stream->exp-stream
               ll
               get-json-or-whitespace
               get-syntax-errors-as-values)))

      (define (parse-json-or x error-value)
         (let ((res (parse-json x)))
            (if (eq? res 'error)
               error-value
               res)))

      (example
         ;; single expressions
         (parse-json "0") = 0
         (parse-json "42") = 42
         (parse-json "-42") = -42
         (parse-json "0.0") = 0
         (parse-json "-0.0") = 0
         (parse-json "-0.0e1") = 0
         (parse-json "  0.001") = 0.001
         (parse-json "-12.34  ") = -12.34
         (parse-json " 1.23e2 ") = 1.23e2
         (parse-json "-1.23e10") = -1.23e10
         (parse-json "[]") = (vector)
         (parse-json "[1]") = (vector 1)
         (parse-json "[1, 2, 3]") = (vector 1 2 3)
         (parse-json "{}") = '()
         (parse-json "{\"foo\": 42}") = '((foo . 42))
         (parse-json "{\"foo\": 42, \"bar\": 43}") = '((foo . 42) (bar . 43))
         (parse-json "\"a\\nb\"") = "a\nb"
         (parse-json "\"\\u03BB\"") = "λ"
         (parse-json "\"\\uD834\\uDD1E\"") = (list->string (list #x1D11E)) ;;   𝄞

         ;; sequences
         (force-ll (byte-stream->json-stream (string->bytes "1"))) = (list 1)
         (force-ll (byte-stream->json-stream (string->bytes "  1  "))) = (list 1)
         (force-ll (byte-stream->json-stream (string->bytes "  1 \n\t 2 \r\n"))) = (list 1 2)
      )

))

;(import (owl vector-fun))
;(import (owl json))
;(print (vector->list (parse-json "[1, 2, 3, 4]")))
